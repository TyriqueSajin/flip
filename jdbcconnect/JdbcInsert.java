package jdbcconnect;

import java.sql.*;  
class JdbcInsert{  
public static void main(String args[]){  
try{  
	Class.forName("com.mysql.cj.jdbc.Driver");
	Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","1234");   

PreparedStatement stmt=con.prepareStatement("insert into cric values(?,?,?)");  
stmt.setInt(1,123); 
stmt.setString(2,"John");  
stmt.setFloat(3, 24000.17f);
  
int i=stmt.executeUpdate();  
System.out.println(i+" records inserted");  
  
con.close();  
  
}catch(Exception e){ System.out.println(e);}  
  
}  
}  